import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'search_result.dart';

class SearchView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return new SearchViewState();
  }
}

class SearchViewState extends State<SearchView> {
  String _drugsEndPoint =
      "https://pharma-enduser-bff.appspot.com/api/v1/search/";
  final TextEditingController _controller = new TextEditingController();
  String _text = "";
  var drugItems;

  void _onChanged(String userInput) {
    setState(() {
      _text = userInput;
    });
  }

  void _showAlertDialog(String value) {
    if (value.isEmpty) {
      return;
    }

    AlertDialog alertDialog = new AlertDialog(
      content: new Row(
        children: <Widget>[
          new CircularProgressIndicator(),
          new Container(
            width: 10.0,
            height: 10.0,
          ),
          new Text("Searching..."),
        ],
      ),
    );

    showDialog(context: context, child: alertDialog);
  }

  _fetchSearchData(String searchResultUrl, BuildContext context) async {
    final response =  await http.get(searchResultUrl);
    if(response.statusCode == 200) {

      final map = json.decode(response.body);
      final drugItemsMap = map["drugItems"];
      final drugItemSuggestions = map["drugItemSuggestions"];

      print("drug $drugItemsMap <> $drugItemSuggestions");

      Navigator.pop(context);

      Navigator.push(context, new MaterialPageRoute(builder: (context) => new SearchResult(drugItem: drugItemsMap, drugItemSuggestion: drugItemSuggestions,)
      ));
    }

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
        child: new Stack(
      children: <Widget>[
        new Card(
          margin: new EdgeInsets.only(
              left: 50.0, top: 20.0, right: 30.0, bottom: 0.0),
          child: new Row(
            children: <Widget>[
              new Expanded(
                child: new Padding(
                  child: new TextField(
                    onChanged: (String value) {
                      _onChanged(value);
                    },
                    style: new TextStyle(
                      fontSize: 18.0,
                      color: Colors.black,

                    ),
                    maxLines: 1,
                    controller: _controller,
                    decoration: new InputDecoration(hintText: "Search for drugs"),
                  ),
                  padding: new EdgeInsets.only(left: 10.0),
                ),
                flex: 3,
              ),
              new Expanded(
                child: new RaisedButton(
                  onPressed: () {
                    _showAlertDialog(_text);
                    print(_drugsEndPoint + _text);
                    _fetchSearchData(_drugsEndPoint + _text, context);

                  },
                  color: Colors.greenAccent[700],
                  child: new Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                ),
                flex: 1,
              ),
            ],
          ),
        ),
      ],
    ));
  }
}


