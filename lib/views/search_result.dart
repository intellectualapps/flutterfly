import 'package:flutter/material.dart';

class SearchResult extends StatelessWidget{
  var drugItem;
  var drugItemSuggestion;

  SearchResult({this.drugItem, this.drugItemSuggestion});


  _showBottomSheet(BuildContext context){
    showModalBottomSheet(context: context, builder: (BuildContext context) {
      //return new DrugCell(this.drugItemSuggestion);
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          backgroundColor: Colors.black,
          title: new Text("Search result",),
        ),
        body: new Column(
          children: <Widget>[

            new Expanded(child: new DrugCell(drugItems: this.drugItem,drugItemsSuggestion: this.drugItemSuggestion,))
          ],
        )
      ),
    );

  }

}

class DrugCell extends StatelessWidget{
  final drugItems;
  final drugItemsSuggestion;

  DrugCell({this.drugItems, this.drugItemsSuggestion});

  @override
  Widget build(BuildContext context) {

    return new Column(
      children: <Widget>[

        new Expanded(child: new ListView.builder(
          itemCount: this.drugItems != null ? this.drugItems.length : new Text("Sorry no result found"),
          itemBuilder: (context, index){
            final drugItem = this.drugItems[index];

            return new Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0),
              child: new Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                new Container(
                  padding: new EdgeInsets.only(left: 20.0, top: 30.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(drugItem["name"], style: new TextStyle(fontSize: 20.0),),
                      new Padding(
                        padding: new EdgeInsets.only(top: 30.0, bottom: 30.0),
                        child: new Row(
                          children: <Widget>[
                            new Text(drugItem["pharmacyName"], style: new TextStyle(fontSize: 16.0),),

                            new Padding(padding: new EdgeInsets.only(left: 8.0),
                                child: new Text(drugItem["pharmacyAddress"], style: new TextStyle(fontSize: 13.0),)),
                          ],
                        ),
                      ),
                    ],
                  ),),
                new Divider(color: Colors.black,)
              ],),
            );
          },
        )),

        new Divider(color: Colors.black,),
        new Text("Suggestions", style: new TextStyle(color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.bold),),

        new Expanded(child: new ListView.builder(
          itemCount: this.drugItemsSuggestion != null ? this.drugItemsSuggestion.length : new Text("Sorry no result found"),
          itemBuilder: (context, index){
            final drugItem = this.drugItemsSuggestion[index];

            return new Padding(
              padding: new EdgeInsets.only(left: 16.0, right: 16.0),
              child: new Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                new Container(
                  padding: new EdgeInsets.only(left: 20.0, top: 30.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(drugItem["name"], style: new TextStyle(fontSize: 20.0),),
                      new Padding(
                        padding: new EdgeInsets.only(top: 30.0, bottom: 30.0),
                        child: new Row(
                          children: <Widget>[
                            new Text(drugItem["pharmacyName"], style: new TextStyle(fontSize: 16.0),),

                            new Padding(padding: new EdgeInsets.only(left: 8.0),
                                child: new Text(drugItem["pharmacyAddress"], style: new TextStyle(fontSize: 13.0),)),
                          ],
                        ),
                      ),
                    ],
                  ),),
                new Divider(color: Colors.black,)
              ],),
            );
          },
        ))
      ],
    );
  }

}