import 'package:flutter/material.dart';
import 'views/search_view.dart';

void main() {
  runApp(new MaterialApp(
    routes: <String, WidgetBuilder>{

    },
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.black,
        title: new Text("Search for drugs"),
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
                accountName: new Text("Raynold"),
                accountEmail: new Text("Raynold49@gmail.com"),
                currentAccountPicture: new Icon(Icons.person, size: 70.0,),
                decoration: new BoxDecoration(color: Colors.grey,),
            ),

            new ListTile(
              title: new Text("Find drugs"),
              leading: new Icon(Icons.search),
              onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
                return new MyApp();
              })),
            ),
            new ListTile(
              title: new Text("Profile"),
              leading: new Icon(Icons.person),
              onTap: (){
                //Navigator.of(context).pop();
                Scaffold.of(this.context).showSnackBar(new SnackBar(content: new Text("About Selected")));
              },
            ),
            new ListTile(
              title: new Text("Pharmacies"),
              leading: new Icon(Icons.store),
            ),
            new ListTile(
              title: new Text("My cart"),
              leading: new Icon(Icons.shopping_cart),
            ),
            new ListTile(
              title: new Text("Order history"),
              leading: new Icon(Icons.chrome_reader_mode),
            ),
            new ListTile(
              title: new Text("About"),
              leading: new Icon(Icons.help),
            )
          ],
        ),
      ),
      body: new SearchView(),
    );
  }
}
